package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn) {
        HashMap<String, Double> hashmap = new HashMap<>();
        hashmap.put("1", 10.0);
        hashmap.put("2", 45.0);
        hashmap.put("3", 20.0);
        hashmap.put("4", 35.0);
        hashmap.put("5", 50.0);

        if (hashmap.containsKey(isbn)) {
            return hashmap.get(isbn);
        }
        return 0;
    }
}
